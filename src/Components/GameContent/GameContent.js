import React from 'react';
import './game.css';

const GameContent = () => {
  return (
    <div className="gm4html5_div_class" id="gm4html5_div_id">
      <canvas id="canvas" width="600" height="600" >
        <p>Your browser doesn't support HTML5 canvas.</p>
      </canvas>
    </div>
  );
};

export default GameContent;