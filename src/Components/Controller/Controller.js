import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import NameInput from '../Views/NameInput';
import PickColor from '../Views/PickColor';
import useGMEvent from '../hooks/useGMEvent';
import Ready from '../Views/Ready';
import Game from '../Views/Game';

const Controller = () => {
  const { online } = useGMEvent();

  return (
    <Switch>
      <Route exact path='/'>
        <NameInput />
      </Route>
      {
        online ?
        <Route exact path='/colors'>
          <PickColor />
        </Route>
        : null
      }
      {
        online ?
        <Route exact path='/ready'>
          <Ready />
        </Route>
        : null
      }
      {
        online ?
        <Route path='/game'>
          <Game />
        </Route>
        : null
      }
      <Route>
        <Redirect to='/' />
      </Route>
    </Switch>
  );
};

export default Controller;