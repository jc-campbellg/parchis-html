import React, { useContext } from 'react';
import { Modal, ModalBody } from 'reactstrap';
import { GameContext } from '../../context/GameContext';

const LoadingModal = () => {
  const [state] = useContext(GameContext);
  
  return (
    <Modal isOpen={state.loading} backdrop="static" >
      <ModalBody className='text-center'>
        Cargando...
      </ModalBody>
    </Modal>
  );
};
  
export default LoadingModal;