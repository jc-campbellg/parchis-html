import { useContext } from "react";
import { GameContext } from "../../context/GameContext";

const useModal = () => {
  const [{modal}, actions] = useContext(GameContext);

  return {
    isOpen: modal.show,
    toggle: actions.toggleModal,
    message: modal.message
  };
};

export default useModal;