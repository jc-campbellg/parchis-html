import React, { useContext } from 'react';
import { FaUser, FaUserSlash } from 'react-icons/fa';
import { Col, Button, Row } from 'reactstrap';
import { GameContext } from '../../context/GameContext';

const PickColor = () => {
  const [{players, name}, {setColor, spinner}] = useContext(GameContext);

  const colorsMap = players.map((p, i) => {
    const onClick = () => {
      spinner(true);
      setColor(i);
      window.gml_Script_gmcallback_send_join(null, null, name, i);
    };
    return (
      <Col key={`color${i}`} xs={4} className='mb-3'>
        <Button block color={p.color} onClick={onClick} className='py-3'>
          <span className='d-block'>{ p.available ? <FaUser color='white' /> : <FaUserSlash color='white' /> }</span>
        </Button>
      </Col>
    );
  });

  return (
    <Col className='text-center'>
      <Row className='mb-3'>
        <Col className='text-white'>Seleciona un color disponible:</Col>
      </Row>
      <Row>
        {colorsMap}
      </Row>
    </Col>
  );
};

export default PickColor;