import { useEffect, useContext } from "react";
import { colors } from '../../constants';
import { useHistory } from "react-router-dom";
import { GameContext } from "../../context/GameContext";
import * as $ from 'jquery';

const stringToBool = (parse, bools) => {
  let newParse = {};
  Object.keys(parse).forEach(key => {
    if (bools.includes(key)) {
      newParse[key] = parse[key] === "true" ? true : false;
    } else {
      newParse[key] = parse[key];
    }
  });
  return newParse;
};

const stringToInt = (parse, numbers) => {
  let newParse = {};
  Object.keys(parse).forEach(key => {
    if (numbers.includes(key)) {
      newParse[key] = parseInt(parse[key]);
    } else {
      newParse[key] = parse[key];
    }
  });
  return newParse;
};

const useGMEvent = () => {
  const history = useHistory();
  const [state, actions] = useContext(GameContext);
  const {name, color} = state;

  useEffect(() => {
    const {
      setPlayers, addPlayer, removePlayer,
      openModal, spinner, addChat, setTurn
    } = actions;
    const onGmEvent = ({detail}) => {
      const {type, data} = detail;
      switch (type) {
        case "syncNames":
          spinner(false);
          setPlayers(data.map((d, i) => {
            var p = JSON.parse(d);
            p.color = colors[i];
            return stringToBool(p, ["available", "ready"]);
          }));
          break;
        case "newJoin":
          var d = stringToInt(JSON.parse(data), ["color"]);
          addPlayer(d);
          break;
        case "join":
          actions.editPlayer(color, 'name', name);
          spinner(false);
          history.replace('/ready');
          break;
        case "tryAgainColor":
          history.replace('/colors');
          spinner(false);
          openModal("Seleciona otro color, ya esta ocupado.");
          break;
        case "removePlayer":
          var c = parseInt(data);
          removePlayer(c);
          break;
        case "ready":
          actions.editPlayer(parseInt(data), 'ready', true);
          break;
        case "gameStart":
          const t = parseInt(data);
          setTurn(t);
          openModal("El juego ha comenzado.");
          if (t === color) {
            openModal("Tu comienzas! Que suerte!");
          }
          break;
        case "chat":
          addChat(data);
          let target = $('html,body');
          target.animate({scrollTop: target.height()}, 1000);
          break;
        default:
          break;
      }
    };
    window.addEventListener("gmEvent", onGmEvent);
    return () => {
      window.removeEventListener("gmEvent", onGmEvent);
    };
  }, [actions, history, name, color]);

  return {
    online: state.players.length > 0
  };
};

export default useGMEvent;