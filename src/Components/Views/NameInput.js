import React from 'react';
import { Col, Button, Row, Input, FormText } from 'reactstrap';
import useNameInput from '../hooks/useNameInput';

const NameInput = () => {
  const { name, button} = useNameInput();
  return (
    <Col className='text-center fixed-bottom pb-5 bg-secondary'>
      <Row>
        <Col>
          <FormText color='white'>Un nombre entre 3 a 10 caracteres</FormText>
          <Input type='text' bsSize='lg' placeholder='Nombre' {...name} />
        </Col>
      </Row>
      <Row className='mt-3'>
        <Col className='text-center'>
          <Button {...button} block size='lg' color='light' outline>Siguiente</Button>
        </Col>
      </Row>
    </Col>
  );
};

export default NameInput;