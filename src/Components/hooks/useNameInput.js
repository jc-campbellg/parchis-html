import { useContext } from "react";
import { useHistory } from "react-router-dom";
import validate from 'validate.js';
import { GameContext } from "../../context/GameContext";

const useName = () => {
  const [{name}, {setName}] = useContext(GameContext);
  const history = useHistory();

  const contrains = {
    presence: true,
    length: {
      minimum: 3,
      maximum: 10
    }
  };

  const goToColors = () => {
    history.replace('/colors');
  };

  const error = validate.single(name, contrains);
  const invalid = error ? true : false;

  return {
    name: {
      value: name,
      invalid: invalid,
      onChange: (evt) => setName(evt.currentTarget.value)
    },
    button: {
      onClick: goToColors,
      disabled: invalid
    }
  };
};

export default useName;