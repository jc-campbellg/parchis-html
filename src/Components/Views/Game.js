import React, { useContext, useState, useEffect } from 'react';
import { FaUser, FaUserFriends, FaClock, FaDice } from 'react-icons/fa';
import { MdCheckBox, MdCheckBoxOutlineBlank } from 'react-icons/md';
import { Col, Button, Row, Badge, Input, Card } from 'reactstrap';
import { GameContext } from '../../context/GameContext';
import { Switch, Route, useHistory } from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';
import { colors } from '../../constants';
import dice1 from '../../assets/dice_1.png';
import dice2 from '../../assets/dice_2.png';
import dice3 from '../../assets/dice_3.png';
import dice4 from '../../assets/dice_4.png';
import dice5 from '../../assets/dice_5.png';
import dice6 from '../../assets/dice_6.png';

const Game = () => {
  const [chatBox, setChatBox] = useState('');
  const [hasRoll, setHasRoll] = useState(false);
  const [isRolling, setIsRolling] = useState(false);
  const [{players, dice, turn, start, chat, color, name, diceUsed}, {setDice, setDiceUsed}] = useContext(GameContext);
  const history = useHistory();

  const diceImg = [dice1, dice2, dice3, dice4, dice5, dice6];

  const playersMap = players.map((p, i) => {
    return (
      <Row key={`player${i}`}>
        <Col className='mb-1'>
          <Badge color={p.color} className='w-100 text-left py-1' style={{fontSize: '16px'}}>
            <Row className='m-0'>
              <Col className='w-fc p-0'>
                <FaUser color='inherit' className='mr-3' />
              </Col>
              <Col className='p-0 text-center'>{p.name}</Col>
              {
                !start ? <Col className='w-fc p-0'>{p.ready ? <MdCheckBox color='inherit' /> : <MdCheckBoxOutlineBlank color='inherit' /> }</Col> : null
              }
              <Col className='w-fc p-0'>{p.turn ? <FaClock color='inherit' /> : null }</Col>
            </Row>
          </Badge>  
        </Col>
      </Row>
    );
  });

  const chatMap = chat.map((c, i) => {
    return (
      <Row className='align-items-center mb-2' key={`chat_${i}`}>
        <Col>
          <Card className='text-left px-2' style={{borderRadius: '0 5px 5px 10px'}}>
            {ReactHtmlParser(c)}
          </Card>
        </Col>
      </Row>
    )
  });

  const goToPlayers = () => {
    if (history.location.pathname === '/game/players') {
      history.push('/game/chat');
    } else {
      history.push('/game/players');
    }
  };

  const showChat = () => {
    if (history.location.pathname !== '/game/chat') {
      history.push('/game/chat');
    }
  };

  const send = (e) => {
    if (e.key === 'Enter') {
      window.gmcallback_send_chat(null, null, `<p><b class='text-${colors[color]}'>${name}:</b> ${chatBox}</i></p>`);
      setChatBox('');
    }
  };

  const goToDice = () => {
    if (history.location.pathname !== '/game/dice') {
      history.push('/game/dice');
      if (!hasRoll && !isRolling) {
        setIsRolling(true);
        var t = 25;
        const rollAnim = () => {
          const d1 = Math.round(Math.random()*5+1);
          const d2 = Math.round(Math.random()*5+1);
          setDice(d1, d2);
          t = t-1;
          if (t > 0) {
            setTimeout(rollAnim, 20);
          } else {
            setIsRolling(false);
            setHasRoll(true);
          }
        };
        rollAnim();
      }
    }
  };

  useEffect(() => {
    if (turn !== color) {
      if (history.location.pathname === '/game/dice') {
        history.push('/game/chat');
      }
    } else {
      setHasRoll(false);
      setIsRolling(false);
    }
  }, [turn, history, color]);

  return (
    <Col className='text-center pt-2' style={{marginBottom: '60px'}}>
      <Switch>
        <Route exact path='/game/players'>
          <Row>
            <Col>
              {playersMap}
            </Col>
          </Row>
        </Route>
        <Route exact path='/game/chat'>
          <Row>
            <Col>
              {chatMap}
            </Col>
          </Row>
        </Route>
        <Route exact path='/game/dice'>
          <Row>
            <Col>
              <Button onClick={() => setDiceUsed(1)}>
                <img src={diceImg[dice[0]-1]} alt='dice1' style={{opacity: diceUsed[0] ? '0.5' : '1'}} />
              </Button>
            </Col>
            <Col>
              <Button onClick={() => setDiceUsed(2)}>
                <img src={diceImg[dice[1]-1]} alt='dice2' style={{opacity: diceUsed[1] ? '0.5' : '1'}} />
              </Button>
            </Col>
          </Row>
          {
            hasRoll ?
            <Row className='mt-1'>
              <Col className='text-white'>{`Sacaste ${dice[0]} y ${dice[1]}`}</Col>
            </Row>
            : null
          }
        </Route>
      </Switch>
      <Row className='fixed-bottom px-3 py-2 bg-light'>
        <Col className='w-fc pr-0'>
          <Button disabled={isRolling} onClick={goToPlayers} block color='primary'><FaUserFriends color='inherit' /></Button>
        </Col>
        <Col className='w-fc pr-0'>
          <Button disabled={turn !== color} onClick={goToDice} block color='success'><FaDice size={18} color='inherit' /></Button>
        </Col>
        <Col>
          <Input disabled={isRolling} onFocus={showChat} value={chatBox} onChange={(e) => setChatBox(e.currentTarget.value)} onKeyDown={send} placeholder='Enter para enviar' />
        </Col>
      </Row>
    </Col>
  );
};

export default Game;