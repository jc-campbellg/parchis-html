import React, { useContext } from 'react';
import { FaUser } from 'react-icons/fa';
import { MdCheckBox, MdCheckBoxOutlineBlank } from 'react-icons/md';
import { Col, Button, Row, Badge } from 'reactstrap';
import { GameContext } from '../../context/GameContext';
import { useHistory } from 'react-router-dom';
import { colors } from '../../constants';

const Ready = () => {
  const [{players, color, ready, name}, {setReady}] = useContext(GameContext);
  const history = useHistory();

  const clickReady = () => {
    setReady();
    window.gmcallback_send_chat(null, null, `<i class='text-muted'>Bienvenido <span class='text-${colors[color]}'>${name}</span></i>`)
    window.gml_Script_gmcallback_send_ready(null, null, color);
    history.replace('/game/chat')
  };

  const playersMap = players.map((p, i) => {
    return (
      <Row key={`player${i}`}>
        <Col className='mb-1'>
          <Badge color={p.color} className='w-100 text-left py-1' style={{fontSize: '16px'}}>
            <Row className='m-0'>
              <Col className='w-fc p-0'>
                <FaUser color='inherit' className='mr-3' />
              </Col>
              <Col className='p-0 text-center'>{p.name}</Col>
              <Col className='w-fc p-0'>{p.ready ? <MdCheckBox color='inherit' /> : <MdCheckBoxOutlineBlank color='inherit' /> }</Col>
            </Row>
          </Badge>
        </Col>
      </Row>
    );
  });

  return (
    <Col className='text-center pt-2' style={{marginBottom: '60px'}}>
      {playersMap}
      <Row className='fixed-bottom px-3 py-2 bg-light'>
        <Col>
          <Button onClick={clickReady} disabled={ready} block color='success'>Estoy Listo</Button>
        </Col>
      </Row>
    </Col>
  );
};

export default Ready;