import React from 'react';
import { Modal, ModalBody, ModalFooter, Button } from 'reactstrap';
import useModal from '../hooks/useModal';

const GameModal = () => {
  const { message, ...modalProps} = useModal();
  
  return (
    <Modal {...modalProps} >
      <ModalBody>
        {message}
      </ModalBody>
      <ModalFooter>
        <Button className='px-5' onClick={modalProps.toggle} color='success'>¡Listo!</Button>
      </ModalFooter>
    </Modal>
  );
};
  
export default GameModal;