import React, { useLayoutEffect, useState } from 'react';
import './App.css';
import GameContent from './Components/GameContent/GameContent';
import Controller from './Components/Controller/Controller';
import { Row, Container, Col } from 'reactstrap';
import { BrowserRouter } from 'react-router-dom';
import { GameContextProvider } from './context/GameContext';
import GameModal from './Components/GameModal/GameModal';
import LoadingModal from './Components/GameModal/LoadingModal';
import * as $ from 'jquery';

function App() {
  const [h, setH] = useState();
  useLayoutEffect(() => {
    const h = $(`#game-view`).height();
    setH(h);
  }, [setH]);
  return (
    <BrowserRouter>
    <GameContextProvider>
      <Container fluid className='d-flex p-0 flex-column bg-secondary'>
        <Row className='m-0 w-100 fixed-top'>
          <Col className='p-0' id='game-view'>
            <GameContent />
          </Col>
        </Row>
        <Row className='mx-0 bg-secondary align-items-center justify-content-center overflow-hidden' style={{marginTop: h+"px"}}>
          <Controller />
        </Row>
      </Container>
      <GameModal />
      <LoadingModal />
    </GameContextProvider>
    </BrowserRouter>
  );
}

export default App;
