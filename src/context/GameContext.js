import React, { createContext, useReducer } from "react";

export const GameContext = createContext();

const initialState = {
  players: [],
  name: '',
  turn: 0,
  color: null,
  ready: false,
  modal: {
    show: false,
    message: 'Bienvenido',
    error: false
  },
  loading: true,
  start: false,
  chat: [],
  dice: [6,6],
  diceUsed: [false, false]
};

const reducer = (state, action) => {
  switch(action.type) {
    case 'SET_NAME':
      return {
        ...state,
        name: action.payload
      };

    case 'SET_COLOR':
      return {
        ...state,
        color: action.payload
      };

    case 'SET_READY':
      return {
        ...state,
        ready: true
      };

    case 'SET_PLAYERS':
      return {
        ...state,
        players: action.payload
      };

    case 'EDIT_PLAYER':
      return {
        ...state,
        players: state.players.map((p, c) => {
          if (c !== action.payload.color) return p;
          return {
            ...p,
            [action.payload.key]: action.payload.value
          }
        })
      };

    case 'REMOVE_PLAYER':
      var c = action.payload;
      return {
        ...state,
        players: state.players.map((p, i) => {
          if (i !== c) return p;
          return {
            ...p,
            name: '',
            available: true,
            ready: false
          }
        })
      };

    case 'ADD_PLAYER':
      var data = action.payload;
      return {
        ...state,
        players: state.players.map((p, i) => {
          if (i !== data.color) return p;
          return {
            ...p,
            name: data.name,
            available: false
          }
        })
      };

    case 'SET_MODAL':
      return {
        ...state,
        modal: {
          ...state.modal,
          ...action.payload
        }
      };
    
    case 'SET_LOADING':
      return {
        ...state,
        loading: action.payload
      };

    case 'ADD_CHAT':
      return {
        ...state,
        chat: [...state.chat, action.payload]
      };
    
    case 'SET_TURN':
      return {
        ...state,
        turn: action.payload
      };
    
    case 'SET_DICE':
      return {
        ...state,
        dice: action.payload,
        diceUsed: [false, false]
      };
    
    case 'SET_DICE_USED':
      return {
        ...state,
        diceUsed: state.diceUsed.map((d,i) => {
          if (i !== action.payload) return d;
          return true;
        })
      };

    default:
      return state;
  }
};

export const GameContextProvider = (props) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const actions = {
    setDice: (d1, d2) => dispatch({
      type: 'SET_DICE',
      payload: [d1, d2]
    }),
    setDiceUsed: (i) => dispatch({
      type: 'SET_DICE_USED',
      payload: i-1
    }),
    setTurn: (t) => dispatch({
      type: 'SET_TURN',
      payload: t
    }),
    addChat: (msg) => dispatch({
      type: 'ADD_CHAT',
      payload: msg
    }),
    setName: (name) => dispatch({
      type: 'SET_NAME',
      payload: name
    }),
    setColor: (color) => dispatch({
      type: 'SET_COLOR',
      payload: color
    }),
    setReady: () => dispatch({
      type: 'SET_READY',
    }),
    setPlayers: (players) => dispatch({
      type: 'SET_PLAYERS',
      payload: players
    }),
    removePlayer: (c) => dispatch({
      type: 'REMOVE_PLAYER',
      payload: c
    }),
    addPlayer: (data) => dispatch({
      type: 'ADD_PLAYER',
      payload: data
    }),
    editPlayer: (color, key, value) => dispatch({
      type: 'EDIT_PLAYER',
      payload: {
        color: color,
        key: key,
        value: value
      }
    }),
    openModal: (message) => dispatch({
      type: 'SET_MODAL',
      payload: {
        show: true,
        message: message,
        error: false
      }
    }),
    toggleModal: (message) => dispatch({
      type: 'SET_MODAL',
      payload: {
        show: !state.modal.show,
      }
    }),
    spinner: (show) => dispatch({
      type: 'SET_LOADING',
      payload: show
    })
  };

  return (
    <GameContext.Provider value={[state, actions]}>
      {props.children}
    </GameContext.Provider>
  );
};